﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;

namespace Prelab
{
	class FileOperation
	{
		static public void WriteFile(Staff staffs, char seperated, string filename)
		{
			using (BinaryWriter writer = new BinaryWriter(new FileStream(filename, FileMode.Append)))
			{
				writer.Write(staffs.ToString(seperated));
				writer.Dispose();
				writer.Close();
			}
		}
		static public void ReadFromFile(ref List<Staff> staffs, ref char seperated, string filename)
		{
			try
			{
				using (BinaryReader reader = new BinaryReader(new FileStream(filename, FileMode.Open)))
				{
					int flag = 0;
					string str;
					str = reader.ReadString();
					if (str.Contains('{'))
					{
						reader.Close();
						reader.Dispose();
						List<Staff> stf;
						using (StreamReader r = new StreamReader(filename))
						{
							string json = r.ReadToEnd();
							stf = JsonConvert.DeserializeObject<List<Staff>>(json);
						}
						if (stf != null)
						{
							for (int i = 0; i < stf.Count; i++)
							{
								flag = 0;
								for (int j = 0; j < Program.staff.Count; j++)
								{
									if (Program.staff[j].ID == stf[i].ID)
									{
										flag = 1;
									}
								}
								if (flag == 0)
								{
									Program.staff.Add(stf[i]);
								}
							}
						}
						return;
					}
					else if (str.Contains(','))
					{
						seperated = ',';
					}

					else
					{
						seperated = '\t';
					}
				}
				using (BinaryReader reader = new BinaryReader(new FileStream(filename, FileMode.Open)))
				{
					string str;
					List<Staff> stf = new List<Staff>();
					while (reader.BaseStream.Position != reader.BaseStream.Length)
					{
						str = reader.ReadString();
						string[] strarray = str.Split(seperated);

						int degreeindex = Convert.ToInt32(strarray[6]);
						int index = 7;
						int[] degreeindexes = null;
						if (degreeindex != 0)
						{
							degreeindexes = new int[degreeindex];
							for (int i = 0; i < degreeindex; i++)
							{
								degreeindexes[i] = Convert.ToInt32(strarray[index]);
								index++;
							}
						}
						else
							index++;
						string city = strarray[index++];
						int experienceindex = Convert.ToInt32(strarray[index++]);
						int[] foregnindexes = null;
						int noofforindex = Convert.ToInt32(strarray[index++]);
						if (noofforindex != 0)
						{
							foregnindexes = new int[noofforindex];
							for (int i = 0; i < noofforindex; i++)
							{
								foregnindexes[i] = Convert.ToInt32(strarray[index]);
								index++;
							}
						}
						else
							index++;
						int noofchilren = Convert.ToInt32(strarray[index++]);
						int[] children = null;
						if (noofchilren != 0)
						{
							children = new int[noofchilren];
							for (int i = 0; i < noofchilren; i++)
							{
								children[i] = Convert.ToInt32(strarray[index]);
								index++;
							}
						}
						else
							index++;
						if (Convert.ToInt32(strarray[strarray.Length - 1]) >= 20000000)
						{
							stf.Add(new PartTimeStaff(strarray[0], strarray[1], Convert.ToInt32(strarray[2]), new BMO(Convert.ToInt32(strarray[3]), Convert.ToInt32(strarray[4]), noofchilren, Convert.ToInt32(strarray[5]), degreeindexes, city, experienceindex, foregnindexes, children, Convert.ToInt32(strarray[index++])), Convert.ToInt32(strarray[index])));

						}
						else
							stf.Add(new Staff(strarray[0], strarray[1], Convert.ToInt32(strarray[2]), new BMO(Convert.ToInt32(strarray[3]), Convert.ToInt32(strarray[4]), noofchilren, Convert.ToInt32(strarray[5]), degreeindexes, city, experienceindex, foregnindexes, children, Convert.ToInt32(strarray[index++])), Convert.ToInt32(strarray[index])));

					}
					if (stf.Count != 0)
					{
						int flag = 0;
						for (int i = 0; i < stf.Count; i++)
						{
							flag = 0;
							for (int j = 0; j < Program.staff.Count; j++)
							{
								if (Program.staff[j].ID == stf[i].ID)
								{
									flag = 1;
								}
							}
							if (flag == 0)
							{
								Program.staff.Add(stf[i]);
							}
						}
					}
				}
			}
			catch{ }
		}
		
		static public void WriteJsonFormat(List<Staff> staff, string flnm)
		{
			string json = JsonConvert.SerializeObject(staff);
			File.WriteAllText(flnm, json);
		}
	}
}
