﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using Newtonsoft.Json;
namespace Prelab
{
	static class Program
	{
		static public Dictionary<string, double> city = new Dictionary<string, double>();//percentege for city
		static public Dictionary<string, double> degree = new Dictionary<string, double>();//percentege for degree
		static public Dictionary<string, double> experience = new Dictionary<string, double>();//percentege for experience
		static public Dictionary<string, double> language = new Dictionary<string, double>();//percentege for laguage checboxlist
		static public Dictionary<string, double> position = new Dictionary<string, double>();//percentege for position combobox
		static public Dictionary<string, double> chidren = new Dictionary<string, double>();//percentege for children combobox
		static public int baseSalary = 4500;
		static public int FullTimeStaffIndex = 15210001;
		static public int PartTimeStaffIndex = 25210001;

		static public List<Staff> staff;

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);

			char selected = 'c';//seperated type character
			string filename;//selected file name
			
			//this for the creating a directory in "D:\" directory*********************//
			try
			{
				Directory.CreateDirectory("D:\\StaffManagementSystem");
				
			}
			catch
			{
				using (BinaryReader reader = new BinaryReader(new FileStream("StaffInfoCsv.dat", FileMode.Open)))
				{
					FullTimeStaffIndex = reader.ReadInt32();
					PartTimeStaffIndex = reader.ReadInt32();
				}
			}
			using (BinaryReader reader = new BinaryReader(new FileStream("StaffInfoCsv.dat", FileMode.Open)))
			{
				FullTimeStaffIndex = reader.ReadInt32();
				PartTimeStaffIndex = reader.ReadInt32();
			}
			//*************************************************************************//


			//this for the BMO calculation for city************************************//
			string citystrings = File.ReadAllText(@"cities.dat");
			city = JsonConvert.DeserializeObject<Dictionary<string, double>>(citystrings);
			//*************************************************************************//


			//this for the BMO calculation for degree**********************************//
			string degreestrings = File.ReadAllText(@"degree.dat");
			degree = JsonConvert.DeserializeObject<Dictionary<string, double>>(degreestrings);
			//*************************************************************************//


			//this for the BMO calculation for experince********************************//
			string experiencestrings = File.ReadAllText(@"experience.dat");
			experience = JsonConvert.DeserializeObject<Dictionary<string, double>>(experiencestrings);
			//*************************************************************************//


			//this for the BMO calculation for language checklistbox*******************//
			string languagestrings = File.ReadAllText(@"language.dat");
			language = JsonConvert.DeserializeObject<Dictionary<string, double>>(languagestrings);
			//*************************************************************************//


			//this for the BMO calculation for position********************************//
			string positionstrings = File.ReadAllText(@"position.dat");
			position = JsonConvert.DeserializeObject<Dictionary<string, double>>(positionstrings);
			//*************************************************************************//


			//this for the BMO calculation for children ages***************************//
			string chidrenstrings = File.ReadAllText(@"chidren.dat");
			chidren = JsonConvert.DeserializeObject<Dictionary<string, double>>(chidrenstrings);
			//*************************************************************************//

			//this for the BMO calculation for children ages***************************//
			string basesalarystring = File.ReadAllText(@"basesalary.dat");
			baseSalary = JsonConvert.DeserializeObject<int>(basesalarystring);
			//*************************************************************************//

			//it reads all staffs information from selected file***********************//
			staff = new List<Staff>();			
			//*************************************************************************//


			Application.Run(new StaffList());

			using (BinaryWriter writer = new BinaryWriter(new FileStream(@"StaffInfoCsv.dat", FileMode.Create)))
			{
				writer.Write(FullTimeStaffIndex);
				writer.Write(PartTimeStaffIndex);
			}
		}
	}
}