﻿namespace Prelab
{
	partial class ListForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListForm));
			this.txtbxName = new System.Windows.Forms.TextBox();
			this.txtbxSurname = new System.Windows.Forms.TextBox();
			this.txtbxBMO = new System.Windows.Forms.TextBox();
			this.txtbxSalary = new System.Windows.Forms.TextBox();
			this.txtbxID = new System.Windows.Forms.TextBox();
			this.lbName = new System.Windows.Forms.Label();
			this.lbSurname = new System.Windows.Forms.Label();
			this.lbSalary = new System.Windows.Forms.Label();
			this.lbBMO = new System.Windows.Forms.Label();
			this.lbID = new System.Windows.Forms.Label();
			this.cbxcities = new System.Windows.Forms.ComboBox();
			this.chclstbxdegree = new System.Windows.Forms.CheckedListBox();
			this.lbcities = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.lblanguage = new System.Windows.Forms.Label();
			this.chclstbxdlanguage = new System.Windows.Forms.CheckedListBox();
			this.lbForeginl = new System.Windows.Forms.Label();
			this.txtbxnoofforlang = new System.Windows.Forms.TextBox();
			this.cbxposition = new System.Windows.Forms.ComboBox();
			this.cbxexperience = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.lbposition = new System.Windows.Forms.Label();
			this.rbesevet = new System.Windows.Forms.RadioButton();
			this.rbeshayir = new System.Windows.Forms.RadioButton();
			this.gbes = new System.Windows.Forms.GroupBox();
			this.gbevlilik = new System.Windows.Forms.GroupBox();
			this.rbevlilikhayir = new System.Windows.Forms.RadioButton();
			this.rbevlilikevet = new System.Windows.Forms.RadioButton();
			this.lbNoofchildren = new System.Windows.Forms.Label();
			this.cbxNoofchildren = new System.Windows.Forms.ComboBox();
			this.cbx1stchild = new System.Windows.Forms.ComboBox();
			this.cbx2ndchild = new System.Windows.Forms.ComboBox();
			this.lbchildrens1 = new System.Windows.Forms.Label();
			this.lbchildrens2 = new System.Windows.Forms.Label();
			this.lbuyarı = new System.Windows.Forms.Label();
			this.pctrebxfoto = new System.Windows.Forms.PictureBox();
			this.Addphoto = new System.Windows.Forms.Button();
			this.bttnsave = new System.Windows.Forms.Button();
			this.grbxStatusOfStaff = new System.Windows.Forms.GroupBox();
			this.rdbParttimeStaff = new System.Windows.Forms.RadioButton();
			this.rdbFullTimeStaff = new System.Windows.Forms.RadioButton();
			this.gbes.SuspendLayout();
			this.gbevlilik.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pctrebxfoto)).BeginInit();
			this.grbxStatusOfStaff.SuspendLayout();
			this.SuspendLayout();
			// 
			// txtbxName
			// 
			this.txtbxName.Location = new System.Drawing.Point(111, 62);
			this.txtbxName.Name = "txtbxName";
			this.txtbxName.Size = new System.Drawing.Size(144, 22);
			this.txtbxName.TabIndex = 0;
			// 
			// txtbxSurname
			// 
			this.txtbxSurname.Location = new System.Drawing.Point(363, 62);
			this.txtbxSurname.Name = "txtbxSurname";
			this.txtbxSurname.Size = new System.Drawing.Size(144, 22);
			this.txtbxSurname.TabIndex = 1;
			// 
			// txtbxBMO
			// 
			this.txtbxBMO.Enabled = false;
			this.txtbxBMO.Location = new System.Drawing.Point(363, 90);
			this.txtbxBMO.Name = "txtbxBMO";
			this.txtbxBMO.Size = new System.Drawing.Size(144, 22);
			this.txtbxBMO.TabIndex = 2;
			// 
			// txtbxSalary
			// 
			this.txtbxSalary.Location = new System.Drawing.Point(111, 90);
			this.txtbxSalary.Name = "txtbxSalary";
			this.txtbxSalary.Size = new System.Drawing.Size(144, 22);
			this.txtbxSalary.TabIndex = 2;
			this.txtbxSalary.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtbxSalary_KeyPress);
			// 
			// txtbxID
			// 
			this.txtbxID.Enabled = false;
			this.txtbxID.Location = new System.Drawing.Point(242, 29);
			this.txtbxID.Name = "txtbxID";
			this.txtbxID.Size = new System.Drawing.Size(144, 22);
			this.txtbxID.TabIndex = 2;
			// 
			// lbName
			// 
			this.lbName.AutoSize = true;
			this.lbName.Location = new System.Drawing.Point(42, 67);
			this.lbName.Name = "lbName";
			this.lbName.Size = new System.Drawing.Size(45, 17);
			this.lbName.TabIndex = 3;
			this.lbName.Text = "Name";
			// 
			// lbSurname
			// 
			this.lbSurname.AutoSize = true;
			this.lbSurname.Location = new System.Drawing.Point(285, 67);
			this.lbSurname.Name = "lbSurname";
			this.lbSurname.Size = new System.Drawing.Size(65, 17);
			this.lbSurname.TabIndex = 3;
			this.lbSurname.Text = "Surname";
			// 
			// lbSalary
			// 
			this.lbSalary.AutoSize = true;
			this.lbSalary.Location = new System.Drawing.Point(40, 95);
			this.lbSalary.Name = "lbSalary";
			this.lbSalary.Size = new System.Drawing.Size(48, 17);
			this.lbSalary.TabIndex = 3;
			this.lbSalary.Text = "Salary";
			// 
			// lbBMO
			// 
			this.lbBMO.AutoSize = true;
			this.lbBMO.Enabled = false;
			this.lbBMO.Location = new System.Drawing.Point(277, 95);
			this.lbBMO.Name = "lbBMO";
			this.lbBMO.Size = new System.Drawing.Size(81, 17);
			this.lbBMO.TabIndex = 3;
			this.lbBMO.Text = "BMO salary";
			// 
			// lbID
			// 
			this.lbID.AutoSize = true;
			this.lbID.Location = new System.Drawing.Point(211, 31);
			this.lbID.Name = "lbID";
			this.lbID.Size = new System.Drawing.Size(21, 17);
			this.lbID.TabIndex = 3;
			this.lbID.Text = "ID";
			// 
			// cbxcities
			// 
			this.cbxcities.FormattingEnabled = true;
			this.cbxcities.Location = new System.Drawing.Point(111, 123);
			this.cbxcities.Name = "cbxcities";
			this.cbxcities.Size = new System.Drawing.Size(144, 24);
			this.cbxcities.TabIndex = 3;
			// 
			// chclstbxdegree
			// 
			this.chclstbxdegree.FormattingEnabled = true;
			this.chclstbxdegree.Location = new System.Drawing.Point(111, 340);
			this.chclstbxdegree.Name = "chclstbxdegree";
			this.chclstbxdegree.Size = new System.Drawing.Size(354, 89);
			this.chclstbxdegree.TabIndex = 6;
			// 
			// lbcities
			// 
			this.lbcities.AutoSize = true;
			this.lbcities.Location = new System.Drawing.Point(44, 126);
			this.lbcities.Name = "lbcities";
			this.lbcities.Size = new System.Drawing.Size(42, 17);
			this.lbcities.TabIndex = 10;
			this.lbcities.Text = "Cities";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(37, 340);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(55, 17);
			this.label3.TabIndex = 10;
			this.label3.Text = "Degree";
			// 
			// lblanguage
			// 
			this.lblanguage.AutoSize = true;
			this.lblanguage.Location = new System.Drawing.Point(35, 444);
			this.lblanguage.Name = "lblanguage";
			this.lblanguage.Size = new System.Drawing.Size(72, 17);
			this.lblanguage.TabIndex = 11;
			this.lblanguage.Text = "Language";
			// 
			// chclstbxdlanguage
			// 
			this.chclstbxdlanguage.FormattingEnabled = true;
			this.chclstbxdlanguage.Location = new System.Drawing.Point(111, 444);
			this.chclstbxdlanguage.Name = "chclstbxdlanguage";
			this.chclstbxdlanguage.Size = new System.Drawing.Size(354, 55);
			this.chclstbxdlanguage.TabIndex = 7;
			this.chclstbxdlanguage.SelectedIndexChanged += new System.EventHandler(this.chclstbxdlanguage_SelectedIndexChanged);
			// 
			// lbForeginl
			// 
			this.lbForeginl.AutoSize = true;
			this.lbForeginl.Location = new System.Drawing.Point(34, 508);
			this.lbForeginl.Name = "lbForeginl";
			this.lbForeginl.Size = new System.Drawing.Size(162, 17);
			this.lbForeginl.TabIndex = 13;
			this.lbForeginl.Text = "No of Foreign Language";
			this.lbForeginl.Visible = false;
			// 
			// txtbxnoofforlang
			// 
			this.txtbxnoofforlang.Location = new System.Drawing.Point(202, 508);
			this.txtbxnoofforlang.Name = "txtbxnoofforlang";
			this.txtbxnoofforlang.Size = new System.Drawing.Size(121, 22);
			this.txtbxnoofforlang.TabIndex = 14;
			this.txtbxnoofforlang.Visible = false;
			// 
			// cbxposition
			// 
			this.cbxposition.FormattingEnabled = true;
			this.cbxposition.Location = new System.Drawing.Point(90, 536);
			this.cbxposition.Name = "cbxposition";
			this.cbxposition.Size = new System.Drawing.Size(398, 24);
			this.cbxposition.TabIndex = 8;
			// 
			// cbxexperience
			// 
			this.cbxexperience.FormattingEnabled = true;
			this.cbxexperience.Items.AddRange(new object[] {
            "0-1",
            "2-4",
            "5-9",
            "10-14",
            "15-20",
            "20+"});
			this.cbxexperience.Location = new System.Drawing.Point(363, 123);
			this.cbxexperience.Name = "cbxexperience";
			this.cbxexperience.Size = new System.Drawing.Size(144, 24);
			this.cbxexperience.TabIndex = 4;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(278, 126);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(78, 17);
			this.label1.TabIndex = 17;
			this.label1.Text = "Experience";
			// 
			// lbposition
			// 
			this.lbposition.AutoSize = true;
			this.lbposition.Location = new System.Drawing.Point(26, 539);
			this.lbposition.Name = "lbposition";
			this.lbposition.Size = new System.Drawing.Size(58, 17);
			this.lbposition.TabIndex = 20;
			this.lbposition.Text = "Position";
			// 
			// rbesevet
			// 
			this.rbesevet.AutoSize = true;
			this.rbesevet.Location = new System.Drawing.Point(-1, 21);
			this.rbesevet.Name = "rbesevet";
			this.rbesevet.Size = new System.Drawing.Size(57, 21);
			this.rbesevet.TabIndex = 23;
			this.rbesevet.TabStop = true;
			this.rbesevet.Text = "Evet";
			this.rbesevet.UseVisualStyleBackColor = true;
			this.rbesevet.CheckedChanged += new System.EventHandler(this.esgrupbx);
			// 
			// rbeshayir
			// 
			this.rbeshayir.AutoSize = true;
			this.rbeshayir.Location = new System.Drawing.Point(62, 21);
			this.rbeshayir.Name = "rbeshayir";
			this.rbeshayir.Size = new System.Drawing.Size(62, 21);
			this.rbeshayir.TabIndex = 24;
			this.rbeshayir.TabStop = true;
			this.rbeshayir.Text = "Hayır";
			this.rbeshayir.UseVisualStyleBackColor = true;
			this.rbeshayir.CheckedChanged += new System.EventHandler(this.esgrupbx);
			// 
			// gbes
			// 
			this.gbes.Controls.Add(this.rbeshayir);
			this.gbes.Controls.Add(this.rbesevet);
			this.gbes.Location = new System.Drawing.Point(266, 164);
			this.gbes.Name = "gbes";
			this.gbes.Size = new System.Drawing.Size(143, 49);
			this.gbes.TabIndex = 25;
			this.gbes.TabStop = false;
			this.gbes.Text = "Eşi çalışıyor";
			this.gbes.Visible = false;
			// 
			// gbevlilik
			// 
			this.gbevlilik.Controls.Add(this.rbevlilikhayir);
			this.gbevlilik.Controls.Add(this.rbevlilikevet);
			this.gbevlilik.Location = new System.Drawing.Point(110, 164);
			this.gbevlilik.Name = "gbevlilik";
			this.gbevlilik.Size = new System.Drawing.Size(143, 49);
			this.gbevlilik.TabIndex = 26;
			this.gbevlilik.TabStop = false;
			this.gbevlilik.Text = "Evlilik";
			// 
			// rbevlilikhayir
			// 
			this.rbevlilikhayir.AutoSize = true;
			this.rbevlilikhayir.Location = new System.Drawing.Point(62, 21);
			this.rbevlilikhayir.Name = "rbevlilikhayir";
			this.rbevlilikhayir.Size = new System.Drawing.Size(62, 21);
			this.rbevlilikhayir.TabIndex = 1;
			this.rbevlilikhayir.TabStop = true;
			this.rbevlilikhayir.Text = "Hayır";
			this.rbevlilikhayir.UseVisualStyleBackColor = true;
			this.rbevlilikhayir.CheckedChanged += new System.EventHandler(this.evlilgrupbx);
			// 
			// rbevlilikevet
			// 
			this.rbevlilikevet.AutoSize = true;
			this.rbevlilikevet.Location = new System.Drawing.Point(-1, 21);
			this.rbevlilikevet.Name = "rbevlilikevet";
			this.rbevlilikevet.Size = new System.Drawing.Size(57, 21);
			this.rbevlilikevet.TabIndex = 0;
			this.rbevlilikevet.TabStop = true;
			this.rbevlilikevet.Text = "Evet";
			this.rbevlilikevet.UseVisualStyleBackColor = true;
			this.rbevlilikevet.CheckedChanged += new System.EventHandler(this.evlilgrupbx);
			// 
			// lbNoofchildren
			// 
			this.lbNoofchildren.AutoSize = true;
			this.lbNoofchildren.Location = new System.Drawing.Point(116, 222);
			this.lbNoofchildren.Name = "lbNoofchildren";
			this.lbNoofchildren.Size = new System.Drawing.Size(101, 17);
			this.lbNoofchildren.TabIndex = 27;
			this.lbNoofchildren.Text = "No Of Children";
			// 
			// cbxNoofchildren
			// 
			this.cbxNoofchildren.FormattingEnabled = true;
			this.cbxNoofchildren.Items.AddRange(new object[] {
            "0",
            "1",
            "2+"});
			this.cbxNoofchildren.Location = new System.Drawing.Point(223, 219);
			this.cbxNoofchildren.Name = "cbxNoofchildren";
			this.cbxNoofchildren.Size = new System.Drawing.Size(144, 24);
			this.cbxNoofchildren.TabIndex = 5;
			this.cbxNoofchildren.SelectedIndexChanged += new System.EventHandler(this.cbxNoofchildren_SelectedIndexChanged);
			// 
			// cbx1stchild
			// 
			this.cbx1stchild.FormattingEnabled = true;
			this.cbx1stchild.Items.AddRange(new object[] {
            "0-6 yaş arası çocuk",
            "7-18 yaş arası çocuk",
            "18 yaş üstü çocuk (Üniversite lisans/ön lisans öğrencisi olmak koşuluyla)",
            "18 yaş üstü çocuk"});
			this.cbx1stchild.Location = new System.Drawing.Point(130, 310);
			this.cbx1stchild.Name = "cbx1stchild";
			this.cbx1stchild.Size = new System.Drawing.Size(121, 24);
			this.cbx1stchild.TabIndex = 29;
			this.cbx1stchild.Visible = false;
			// 
			// cbx2ndchild
			// 
			this.cbx2ndchild.FormattingEnabled = true;
			this.cbx2ndchild.Items.AddRange(new object[] {
            "0-6 yaş arası çocuk",
            "7-18 yaş arası çocuk",
            "18 yaş üstü çocuk (Üniversite lisans/ön lisans öğrencisi olmak koşuluyla)",
            "18 yaş üstü çocuk"});
			this.cbx2ndchild.Location = new System.Drawing.Point(265, 310);
			this.cbx2ndchild.Name = "cbx2ndchild";
			this.cbx2ndchild.Size = new System.Drawing.Size(121, 24);
			this.cbx2ndchild.TabIndex = 29;
			this.cbx2ndchild.Visible = false;
			// 
			// lbchildrens1
			// 
			this.lbchildrens1.AutoSize = true;
			this.lbchildrens1.Location = new System.Drawing.Point(132, 284);
			this.lbchildrens1.Name = "lbchildrens1";
			this.lbchildrens1.Size = new System.Drawing.Size(64, 17);
			this.lbchildrens1.TabIndex = 30;
			this.lbchildrens1.Text = "1 st child";
			this.lbchildrens1.Visible = false;
			// 
			// lbchildrens2
			// 
			this.lbchildrens2.AutoSize = true;
			this.lbchildrens2.Location = new System.Drawing.Point(262, 284);
			this.lbchildrens2.Name = "lbchildrens2";
			this.lbchildrens2.Size = new System.Drawing.Size(69, 17);
			this.lbchildrens2.TabIndex = 30;
			this.lbchildrens2.Text = "2 nd child";
			this.lbchildrens2.Visible = false;
			// 
			// lbuyarı
			// 
			this.lbuyarı.AutoSize = true;
			this.lbuyarı.Location = new System.Drawing.Point(114, 254);
			this.lbuyarı.Name = "lbuyarı";
			this.lbuyarı.Size = new System.Drawing.Size(337, 17);
			this.lbuyarı.TabIndex = 31;
			this.lbuyarı.Text = "Lütfen en büyük iki çocuğunuzun yaş bilgisini giriniz.";
			this.lbuyarı.Visible = false;
			// 
			// pctrebxfoto
			// 
			this.pctrebxfoto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.pctrebxfoto.Image = ((System.Drawing.Image)(resources.GetObject("pctrebxfoto.Image")));
			this.pctrebxfoto.Location = new System.Drawing.Point(542, 61);
			this.pctrebxfoto.Name = "pctrebxfoto";
			this.pctrebxfoto.Size = new System.Drawing.Size(156, 194);
			this.pctrebxfoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pctrebxfoto.TabIndex = 32;
			this.pctrebxfoto.TabStop = false;
			// 
			// Addphoto
			// 
			this.Addphoto.Location = new System.Drawing.Point(573, 269);
			this.Addphoto.Name = "Addphoto";
			this.Addphoto.Size = new System.Drawing.Size(94, 32);
			this.Addphoto.TabIndex = 9;
			this.Addphoto.Text = "Add Photo";
			this.Addphoto.UseVisualStyleBackColor = true;
			this.Addphoto.Click += new System.EventHandler(this.button1_Click);
			// 
			// bttnsave
			// 
			this.bttnsave.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.bttnsave.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
			this.bttnsave.Location = new System.Drawing.Point(561, 492);
			this.bttnsave.Name = "bttnsave";
			this.bttnsave.Size = new System.Drawing.Size(118, 42);
			this.bttnsave.TabIndex = 33;
			this.bttnsave.Text = "Save";
			this.bttnsave.UseVisualStyleBackColor = true;
			this.bttnsave.Click += new System.EventHandler(this.bttnsave_Click);
			// 
			// grbxStatusOfStaff
			// 
			this.grbxStatusOfStaff.Controls.Add(this.rdbParttimeStaff);
			this.grbxStatusOfStaff.Controls.Add(this.rdbFullTimeStaff);
			this.grbxStatusOfStaff.Location = new System.Drawing.Point(537, 340);
			this.grbxStatusOfStaff.Name = "grbxStatusOfStaff";
			this.grbxStatusOfStaff.Size = new System.Drawing.Size(166, 100);
			this.grbxStatusOfStaff.TabIndex = 34;
			this.grbxStatusOfStaff.TabStop = false;
			this.grbxStatusOfStaff.Text = "Status Of Staff";
			// 
			// rdbParttimeStaff
			// 
			this.rdbParttimeStaff.AutoSize = true;
			this.rdbParttimeStaff.Location = new System.Drawing.Point(24, 30);
			this.rdbParttimeStaff.Name = "rdbParttimeStaff";
			this.rdbParttimeStaff.Size = new System.Drawing.Size(123, 21);
			this.rdbParttimeStaff.TabIndex = 0;
			this.rdbParttimeStaff.Text = "Part Time Staff";
			this.rdbParttimeStaff.UseVisualStyleBackColor = true;
			// 
			// rdbFullTimeStaff
			// 
			this.rdbFullTimeStaff.AutoSize = true;
			this.rdbFullTimeStaff.Checked = true;
			this.rdbFullTimeStaff.Location = new System.Drawing.Point(24, 57);
			this.rdbFullTimeStaff.Name = "rdbFullTimeStaff";
			this.rdbFullTimeStaff.Size = new System.Drawing.Size(119, 21);
			this.rdbFullTimeStaff.TabIndex = 0;
			this.rdbFullTimeStaff.TabStop = true;
			this.rdbFullTimeStaff.Text = "Full Time Staff";
			this.rdbFullTimeStaff.UseVisualStyleBackColor = true;
			// 
			// ListForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.bttnsave;
			this.ClientSize = new System.Drawing.Size(720, 587);
			this.Controls.Add(this.grbxStatusOfStaff);
			this.Controls.Add(this.bttnsave);
			this.Controls.Add(this.Addphoto);
			this.Controls.Add(this.pctrebxfoto);
			this.Controls.Add(this.lbuyarı);
			this.Controls.Add(this.lbchildrens2);
			this.Controls.Add(this.lbchildrens1);
			this.Controls.Add(this.cbx2ndchild);
			this.Controls.Add(this.cbx1stchild);
			this.Controls.Add(this.cbxNoofchildren);
			this.Controls.Add(this.lbNoofchildren);
			this.Controls.Add(this.gbevlilik);
			this.Controls.Add(this.gbes);
			this.Controls.Add(this.lbposition);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.cbxexperience);
			this.Controls.Add(this.cbxposition);
			this.Controls.Add(this.txtbxnoofforlang);
			this.Controls.Add(this.lbForeginl);
			this.Controls.Add(this.chclstbxdlanguage);
			this.Controls.Add(this.lblanguage);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.lbcities);
			this.Controls.Add(this.chclstbxdegree);
			this.Controls.Add(this.cbxcities);
			this.Controls.Add(this.lbBMO);
			this.Controls.Add(this.lbID);
			this.Controls.Add(this.lbSalary);
			this.Controls.Add(this.lbSurname);
			this.Controls.Add(this.lbName);
			this.Controls.Add(this.txtbxSalary);
			this.Controls.Add(this.txtbxID);
			this.Controls.Add(this.txtbxBMO);
			this.Controls.Add(this.txtbxSurname);
			this.Controls.Add(this.txtbxName);
			this.Name = "ListForm";
			this.Text = "Staffs\' List";
			this.Load += new System.EventHandler(this.ListForm_Load);
			this.gbes.ResumeLayout(false);
			this.gbes.PerformLayout();
			this.gbevlilik.ResumeLayout(false);
			this.gbevlilik.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pctrebxfoto)).EndInit();
			this.grbxStatusOfStaff.ResumeLayout(false);
			this.grbxStatusOfStaff.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.TextBox txtbxName;
		private System.Windows.Forms.TextBox txtbxSurname;
		private System.Windows.Forms.TextBox txtbxBMO;
		private System.Windows.Forms.TextBox txtbxSalary;
		private System.Windows.Forms.TextBox txtbxID;
		private System.Windows.Forms.Label lbName;
		private System.Windows.Forms.Label lbSurname;
		private System.Windows.Forms.Label lbSalary;
		private System.Windows.Forms.Label lbBMO;
		private System.Windows.Forms.Label lbID;
		private System.Windows.Forms.ComboBox cbxcities;
		private System.Windows.Forms.CheckedListBox chclstbxdegree;
		private System.Windows.Forms.Label lbcities;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label lblanguage;
		private System.Windows.Forms.CheckedListBox chclstbxdlanguage;
		private System.Windows.Forms.Label lbForeginl;
		private System.Windows.Forms.TextBox txtbxnoofforlang;
		private System.Windows.Forms.ComboBox cbxposition;
		private System.Windows.Forms.ComboBox cbxexperience;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label lbposition;
		private System.Windows.Forms.RadioButton rbesevet;
		private System.Windows.Forms.RadioButton rbeshayir;
		private System.Windows.Forms.GroupBox gbes;
		private System.Windows.Forms.GroupBox gbevlilik;
		private System.Windows.Forms.RadioButton rbevlilikhayir;
		private System.Windows.Forms.RadioButton rbevlilikevet;
		private System.Windows.Forms.Label lbNoofchildren;
		private System.Windows.Forms.ComboBox cbxNoofchildren;
		private System.Windows.Forms.ComboBox cbx1stchild;
		private System.Windows.Forms.ComboBox cbx2ndchild;
		private System.Windows.Forms.Label lbchildrens1;
		private System.Windows.Forms.Label lbchildrens2;
		private System.Windows.Forms.Label lbuyarı;
		private System.Windows.Forms.PictureBox pctrebxfoto;
		private System.Windows.Forms.Button Addphoto;
		private System.Windows.Forms.Button bttnsave;
		private System.Windows.Forms.GroupBox grbxStatusOfStaff;
		private System.Windows.Forms.RadioButton rdbParttimeStaff;
		private System.Windows.Forms.RadioButton rdbFullTimeStaff;
	}
}