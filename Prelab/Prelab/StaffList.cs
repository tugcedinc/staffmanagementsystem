﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json;
using System.Threading;
namespace Prelab
{
	public partial class StaffList : Form
	{
		public StaffList()
		{
			InitializeComponent();
			listt();

		}
		
		private void listt()
		{

			dtgrdvstafflist.Rows.Clear();
			for (int i = 0; i < Program.staff.Count; i++)
			{
				DataGridViewRow row = (DataGridViewRow)dtgrdvstafflist.Rows[0].Clone();
				row.Cells[0].Value = Program.staff[i].ID;
				row.Cells[1].Value = Program.staff[i].Name;
				row.Cells[2].Value = Program.staff[i].Surname;
				row.Cells[3].Value = Program.staff[i].Salary;
				row.Cells[4].Value = Program.staff[i].calculatebmo();
				dtgrdvstafflist.Rows.Add(row);
			}
		}

		private void bttnAdd_Click(object sender, EventArgs e)
		{
			//int index = dtgrdvstafflist.CurrentCell.RowIndex;
			ListForm form = new ListForm(-1, 1);
			form.ShowDialog();
			listt();

		}

		private void bttnUpdate_Click(object sender, EventArgs e)
		{
			try
			{
				int row = dtgrdvstafflist.CurrentCell.RowIndex;
				int id = Convert.ToInt32(dtgrdvstafflist.Rows[row].Cells["StaffID"].FormattedValue.ToString());
				int index = -1;
				for (int i=0;i<Program.staff.Count;i++)
				{
					if(Program.staff[i].ID==id)
					{
						index = i;
					}
				}
				ListForm form = new ListForm(index, 2);
				form.ShowDialog();
				listt();
			}
			catch
			{
				MessageBox.Show("Please chose a staff to update!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void bttnDelete_Click(object sender, EventArgs e)
		{
			try
			{
				int row = dtgrdvstafflist.CurrentCell.RowIndex;
				int id = Convert.ToInt32(dtgrdvstafflist.Rows[row].Cells["StaffID"].FormattedValue.ToString());

				int deleteindex =-1;
				for (int i = 0; i < Program.staff.Count; i++)
				{
					if (Program.staff[i].ID == id)
					{
						deleteindex = i;
					}
				}
				string ppp = "D:\\StaffManagementSystem\\" + Program.staff[deleteindex].ID.ToString() + ".jpg";
				File.Delete(ppp);
				Program.staff.RemoveAt(deleteindex);
			}
			catch (Exception exp)
			{
				MessageBox.Show(exp.Message);
			}
			listt();

		}

		private void dtgrdvstafflist_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			try
			{
				int row = dtgrdvstafflist.CurrentCell.RowIndex;
				int id = Convert.ToInt32(dtgrdvstafflist.Rows[row].Cells["StaffID"].FormattedValue.ToString());
				int index = -1;
				for (int i = 0; i < Program.staff.Count; i++)
				{
					if (Program.staff[i].ID == id)
					{
						index = i;
					}
				}
				ListForm form = new ListForm(index, 3);
				form.ShowDialog();
				listt();
			}
			catch { }
		}

		private void bttnImport_Click(object sender, EventArgs e)
		{
			prgrsbrforfile.Value = 0;

			prgrsbrforfile.Value += 10;
			OpenFileDialog file = new OpenFileDialog();

			file.Title = "Dosyayı Seçin";
			file.Filter = "dat-file (*.dat) | *.dat";
			string path = "";
			DialogResult result = file.ShowDialog();
			if (result == DialogResult.OK)
			{
				prgrsbrforfile.Value += 10;

				path = file.FileName;
				prgrsbrforfile.Value += 10;

				string filename = path;
				prgrsbrforfile.Value += 10;

				char selected = 'c';
				FileOperation.ReadFromFile(ref Program.staff, ref selected, filename);
				prgrsbrforfile.Value += 50;

				listt();
				prgrsbrforfile.Value += 10;

			}
			file.Dispose();
			Thread.Sleep(1000);
			prgrsbrforfile.Value = 0;
		}

		private void bttnExport_Click(object sender, EventArgs e)
		{
			prgrsbrforfile.Value = 0;
			char selected = 'c';
			SaveFileDialog file = new SaveFileDialog();
			file.Title = "Dosyayı Seçin";
			file.Filter = "TSV (*.dat) | *.dat | CSV (*.dat) | *.dat | json (*.dat) | *.dat ";
			string path = "";
			DialogResult resultfile = file.ShowDialog();
			prgrsbrforfile.Value += 10;
			prgrsbrforfile.Value += 10;

			if (resultfile == DialogResult.OK)
			{
				prgrsbrforfile.Value += 10;

				if (file.FilterIndex == 1 || file.FilterIndex == 2)
				{

					if (file.FilterIndex == 1)
					{
						selected = '\t';

					}
					else
					{
						selected = ',';
					}
					path = file.FileName;
					using (BinaryWriter writer = new BinaryWriter(new FileStream(path, FileMode.Create))) { }
					for (int i = 0; i < Program.staff.Count; i++)
					{
						FileOperation.WriteFile(Program.staff[i], selected, path);
					}
				}
				else if(file.FilterIndex==3)
				{
					FileOperation.WriteJsonFormat(Program.staff, file.FileName);
				}
				prgrsbrforfile.Value += 10;
				prgrsbrforfile.Value += 10;
				prgrsbrforfile.Value += 10;
				prgrsbrforfile.Value += 10;
				prgrsbrforfile.Value += 10;
				prgrsbrforfile.Value += 10;
				prgrsbrforfile.Value += 10;

			}
			listt();

			file.Dispose();
			Thread.Sleep(100);
			prgrsbrforfile.Value = 0;
			
		}

		private void StaffList_Load(object sender, EventArgs e)
		{

		}

		private void dtgrdvstafflist_CellContentClick(object sender, DataGridViewCellEventArgs e)
		{
			
		}

		private void StaffList_FormClosing(object sender, FormClosingEventArgs e)
		{
			DialogResult result = MessageBox.Show("Do you want to close!", "!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (result == DialogResult.Yes)
			{
			
			}
			else if (result == DialogResult.No)
			{
				e.Cancel = true;

			}
		}

		private void cmbxsort_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbxsort.SelectedIndex == 0)
			{
				Program.staff = MergeSort.Mergesort(Program.staff);
			}
			else
			{
				Program.staff = MergeSort.Mergesort(Program.staff);
				Program.staff.Reverse();

			}
			listt();
		}

		private void bttnChangePercentage_Click(object sender, EventArgs e)
		{
			ChangePercentageOfBMO klh = new ChangePercentageOfBMO();
			klh.ShowDialog();

		}
	}
}
