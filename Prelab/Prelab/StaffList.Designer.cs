﻿namespace Prelab
{
	partial class StaffList
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.dtgrdvstafflist = new System.Windows.Forms.DataGridView();
			this.StaffID = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.stafname = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.StaffSurname = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.StaffSalary = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.StaffSalaryBMO = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.bttnDelete = new System.Windows.Forms.Button();
			this.bttnUpdate = new System.Windows.Forms.Button();
			this.bttnAdd = new System.Windows.Forms.Button();
			this.bttnExport = new System.Windows.Forms.Button();
			this.bttnImport = new System.Windows.Forms.Button();
			this.prgrsbrforfile = new System.Windows.Forms.ProgressBar();
			this.cmbxsort = new System.Windows.Forms.ComboBox();
			this.lbsort = new System.Windows.Forms.Label();
			this.bttnChangePercentage = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.dtgrdvstafflist)).BeginInit();
			this.SuspendLayout();
			// 
			// dtgrdvstafflist
			// 
			this.dtgrdvstafflist.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dtgrdvstafflist.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.StaffID,
            this.stafname,
            this.StaffSurname,
            this.StaffSalary,
            this.StaffSalaryBMO});
			this.dtgrdvstafflist.Location = new System.Drawing.Point(12, 12);
			this.dtgrdvstafflist.MultiSelect = false;
			this.dtgrdvstafflist.Name = "dtgrdvstafflist";
			this.dtgrdvstafflist.RowTemplate.Height = 24;
			this.dtgrdvstafflist.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dtgrdvstafflist.Size = new System.Drawing.Size(724, 426);
			this.dtgrdvstafflist.TabIndex = 0;
			this.dtgrdvstafflist.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgrdvstafflist_CellContentClick);
			this.dtgrdvstafflist.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.dtgrdvstafflist_MouseDoubleClick);
			// 
			// StaffID
			// 
			this.StaffID.HeaderText = "Staff ID";
			this.StaffID.Name = "StaffID";
			// 
			// stafname
			// 
			this.stafname.HeaderText = "Staff Name";
			this.stafname.Name = "stafname";
			// 
			// StaffSurname
			// 
			this.StaffSurname.HeaderText = "Staff Surname";
			this.StaffSurname.Name = "StaffSurname";
			// 
			// StaffSalary
			// 
			this.StaffSalary.HeaderText = "Staff Salary";
			this.StaffSalary.Name = "StaffSalary";
			// 
			// StaffSalaryBMO
			// 
			this.StaffSalaryBMO.HeaderText = "Staff Salary(calculated by BMO)";
			this.StaffSalaryBMO.Name = "StaffSalaryBMO";
			// 
			// bttnDelete
			// 
			this.bttnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
			this.bttnDelete.Location = new System.Drawing.Point(305, 444);
			this.bttnDelete.Name = "bttnDelete";
			this.bttnDelete.Size = new System.Drawing.Size(140, 42);
			this.bttnDelete.TabIndex = 17;
			this.bttnDelete.Text = "Delete";
			this.bttnDelete.UseVisualStyleBackColor = true;
			this.bttnDelete.Click += new System.EventHandler(this.bttnDelete_Click);
			// 
			// bttnUpdate
			// 
			this.bttnUpdate.Font = new System.Drawing.Font("Agency FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
			this.bttnUpdate.Location = new System.Drawing.Point(158, 444);
			this.bttnUpdate.Name = "bttnUpdate";
			this.bttnUpdate.Size = new System.Drawing.Size(140, 42);
			this.bttnUpdate.TabIndex = 16;
			this.bttnUpdate.Text = "Update";
			this.bttnUpdate.UseVisualStyleBackColor = true;
			this.bttnUpdate.Click += new System.EventHandler(this.bttnUpdate_Click);
			// 
			// bttnAdd
			// 
			this.bttnAdd.Font = new System.Drawing.Font("Agency FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
			this.bttnAdd.Location = new System.Drawing.Point(12, 444);
			this.bttnAdd.Name = "bttnAdd";
			this.bttnAdd.Size = new System.Drawing.Size(140, 42);
			this.bttnAdd.TabIndex = 15;
			this.bttnAdd.Text = "Add";
			this.bttnAdd.UseVisualStyleBackColor = true;
			this.bttnAdd.Click += new System.EventHandler(this.bttnAdd_Click);
			// 
			// bttnExport
			// 
			this.bttnExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
			this.bttnExport.Location = new System.Drawing.Point(451, 444);
			this.bttnExport.Name = "bttnExport";
			this.bttnExport.Size = new System.Drawing.Size(140, 42);
			this.bttnExport.TabIndex = 17;
			this.bttnExport.Text = "Export";
			this.bttnExport.UseVisualStyleBackColor = true;
			this.bttnExport.Click += new System.EventHandler(this.bttnExport_Click);
			// 
			// bttnImport
			// 
			this.bttnImport.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
			this.bttnImport.Location = new System.Drawing.Point(596, 444);
			this.bttnImport.Name = "bttnImport";
			this.bttnImport.Size = new System.Drawing.Size(140, 42);
			this.bttnImport.TabIndex = 17;
			this.bttnImport.Text = "Import";
			this.bttnImport.UseVisualStyleBackColor = true;
			this.bttnImport.Click += new System.EventHandler(this.bttnImport_Click);
			// 
			// prgrsbrforfile
			// 
			this.prgrsbrforfile.Location = new System.Drawing.Point(12, 492);
			this.prgrsbrforfile.Name = "prgrsbrforfile";
			this.prgrsbrforfile.Size = new System.Drawing.Size(724, 23);
			this.prgrsbrforfile.TabIndex = 18;
			// 
			// cmbxsort
			// 
			this.cmbxsort.FormattingEnabled = true;
			this.cmbxsort.Items.AddRange(new object[] {
            "asc",
            "desc"});
			this.cmbxsort.Location = new System.Drawing.Point(742, 457);
			this.cmbxsort.Name = "cmbxsort";
			this.cmbxsort.Size = new System.Drawing.Size(140, 24);
			this.cmbxsort.TabIndex = 20;
			this.cmbxsort.SelectedIndexChanged += new System.EventHandler(this.cmbxsort_SelectedIndexChanged);
			// 
			// lbsort
			// 
			this.lbsort.AutoSize = true;
			this.lbsort.Location = new System.Drawing.Point(742, 437);
			this.lbsort.Name = "lbsort";
			this.lbsort.Size = new System.Drawing.Size(165, 17);
			this.lbsort.TabIndex = 21;
			this.lbsort.Text = "Sort Salary (asc or desc)";
			// 
			// bttnChangePercentage
			// 
			this.bttnChangePercentage.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
			this.bttnChangePercentage.Location = new System.Drawing.Point(742, 392);
			this.bttnChangePercentage.Name = "bttnChangePercentage";
			this.bttnChangePercentage.Size = new System.Drawing.Size(223, 42);
			this.bttnChangePercentage.TabIndex = 22;
			this.bttnChangePercentage.Text = "Change Percentages";
			this.bttnChangePercentage.UseVisualStyleBackColor = true;
			this.bttnChangePercentage.Click += new System.EventHandler(this.bttnChangePercentage_Click);
			// 
			// StaffList
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(972, 529);
			this.Controls.Add(this.bttnChangePercentage);
			this.Controls.Add(this.lbsort);
			this.Controls.Add(this.cmbxsort);
			this.Controls.Add(this.prgrsbrforfile);
			this.Controls.Add(this.bttnImport);
			this.Controls.Add(this.bttnExport);
			this.Controls.Add(this.bttnDelete);
			this.Controls.Add(this.bttnUpdate);
			this.Controls.Add(this.bttnAdd);
			this.Controls.Add(this.dtgrdvstafflist);
			this.Name = "StaffList";
			this.Text = "StaffList";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.StaffList_FormClosing);
			this.Load += new System.EventHandler(this.StaffList_Load);
			((System.ComponentModel.ISupportInitialize)(this.dtgrdvstafflist)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.DataGridView dtgrdvstafflist;
		private System.Windows.Forms.DataGridViewTextBoxColumn StaffID;
		private System.Windows.Forms.DataGridViewTextBoxColumn stafname;
		private System.Windows.Forms.DataGridViewTextBoxColumn StaffSurname;
		private System.Windows.Forms.DataGridViewTextBoxColumn StaffSalary;
		private System.Windows.Forms.DataGridViewTextBoxColumn StaffSalaryBMO;
		private System.Windows.Forms.Button bttnDelete;
		private System.Windows.Forms.Button bttnUpdate;
		private System.Windows.Forms.Button bttnAdd;
		private System.Windows.Forms.Button bttnExport;
		private System.Windows.Forms.Button bttnImport;
		private System.Windows.Forms.ProgressBar prgrsbrforfile;
		private System.Windows.Forms.ComboBox cmbxsort;
		private System.Windows.Forms.Label lbsort;
		private System.Windows.Forms.Button bttnChangePercentage;
	}
}