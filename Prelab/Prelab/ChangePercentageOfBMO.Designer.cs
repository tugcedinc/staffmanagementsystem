﻿namespace Prelab
{
	partial class ChangePercentageOfBMO
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmbxcity = new System.Windows.Forms.ComboBox();
			this.cmbxdegree = new System.Windows.Forms.ComboBox();
			this.cmbxlanguage = new System.Windows.Forms.ComboBox();
			this.cmbxposition = new System.Windows.Forms.ComboBox();
			this.cmbxchild = new System.Windows.Forms.ComboBox();
			this.cmbxexperience = new System.Windows.Forms.ComboBox();
			this.lbcity = new System.Windows.Forms.Label();
			this.lbdegree = new System.Windows.Forms.Label();
			this.lblanguage = new System.Windows.Forms.Label();
			this.lbposition = new System.Windows.Forms.Label();
			this.lbchild = new System.Windows.Forms.Label();
			this.lbexperience = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.txtbxBaseSalary = new System.Windows.Forms.TextBox();
			this.lbBaseSalary = new System.Windows.Forms.Label();
			this.txtbxShowper = new System.Windows.Forms.TextBox();
			this.lbPercentage = new System.Windows.Forms.Label();
			this.bttnSave = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// cmbxcity
			// 
			this.cmbxcity.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.cmbxcity.FormattingEnabled = true;
			this.cmbxcity.Location = new System.Drawing.Point(174, 69);
			this.cmbxcity.Name = "cmbxcity";
			this.cmbxcity.Size = new System.Drawing.Size(487, 28);
			this.cmbxcity.TabIndex = 0;
			this.cmbxcity.SelectedIndexChanged += new System.EventHandler(this.AllComboBoxes_SelectedIndexChanged);
			// 
			// cmbxdegree
			// 
			this.cmbxdegree.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.cmbxdegree.FormattingEnabled = true;
			this.cmbxdegree.Location = new System.Drawing.Point(174, 118);
			this.cmbxdegree.Name = "cmbxdegree";
			this.cmbxdegree.Size = new System.Drawing.Size(487, 28);
			this.cmbxdegree.TabIndex = 1;
			this.cmbxdegree.SelectedIndexChanged += new System.EventHandler(this.AllComboBoxes_SelectedIndexChanged);
			// 
			// cmbxlanguage
			// 
			this.cmbxlanguage.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.cmbxlanguage.FormattingEnabled = true;
			this.cmbxlanguage.Location = new System.Drawing.Point(174, 167);
			this.cmbxlanguage.Name = "cmbxlanguage";
			this.cmbxlanguage.Size = new System.Drawing.Size(487, 28);
			this.cmbxlanguage.TabIndex = 2;
			this.cmbxlanguage.SelectedIndexChanged += new System.EventHandler(this.AllComboBoxes_SelectedIndexChanged);
			// 
			// cmbxposition
			// 
			this.cmbxposition.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.cmbxposition.FormattingEnabled = true;
			this.cmbxposition.Location = new System.Drawing.Point(174, 216);
			this.cmbxposition.Name = "cmbxposition";
			this.cmbxposition.Size = new System.Drawing.Size(487, 28);
			this.cmbxposition.TabIndex = 3;
			this.cmbxposition.SelectedIndexChanged += new System.EventHandler(this.AllComboBoxes_SelectedIndexChanged);
			// 
			// cmbxchild
			// 
			this.cmbxchild.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.cmbxchild.FormattingEnabled = true;
			this.cmbxchild.Location = new System.Drawing.Point(174, 265);
			this.cmbxchild.Name = "cmbxchild";
			this.cmbxchild.Size = new System.Drawing.Size(487, 28);
			this.cmbxchild.TabIndex = 4;
			this.cmbxchild.SelectedIndexChanged += new System.EventHandler(this.AllComboBoxes_SelectedIndexChanged);
			// 
			// cmbxexperience
			// 
			this.cmbxexperience.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.cmbxexperience.FormattingEnabled = true;
			this.cmbxexperience.Location = new System.Drawing.Point(174, 314);
			this.cmbxexperience.Name = "cmbxexperience";
			this.cmbxexperience.Size = new System.Drawing.Size(487, 28);
			this.cmbxexperience.TabIndex = 4;
			this.cmbxexperience.SelectedIndexChanged += new System.EventHandler(this.AllComboBoxes_SelectedIndexChanged);
			// 
			// lbcity
			// 
			this.lbcity.AutoSize = true;
			this.lbcity.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.lbcity.Location = new System.Drawing.Point(54, 72);
			this.lbcity.Name = "lbcity";
			this.lbcity.Size = new System.Drawing.Size(38, 20);
			this.lbcity.TabIndex = 5;
			this.lbcity.Text = "City";
			// 
			// lbdegree
			// 
			this.lbdegree.AutoSize = true;
			this.lbdegree.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.lbdegree.Location = new System.Drawing.Point(54, 121);
			this.lbdegree.Name = "lbdegree";
			this.lbdegree.Size = new System.Drawing.Size(64, 20);
			this.lbdegree.TabIndex = 5;
			this.lbdegree.Text = "Degree";
			// 
			// lblanguage
			// 
			this.lblanguage.AutoSize = true;
			this.lblanguage.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.lblanguage.Location = new System.Drawing.Point(54, 170);
			this.lblanguage.Name = "lblanguage";
			this.lblanguage.Size = new System.Drawing.Size(82, 20);
			this.lblanguage.TabIndex = 5;
			this.lblanguage.Text = "Language";
			// 
			// lbposition
			// 
			this.lbposition.AutoSize = true;
			this.lbposition.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.lbposition.Location = new System.Drawing.Point(54, 219);
			this.lbposition.Name = "lbposition";
			this.lbposition.Size = new System.Drawing.Size(69, 20);
			this.lbposition.TabIndex = 5;
			this.lbposition.Text = "Position";
			// 
			// lbchild
			// 
			this.lbchild.AutoSize = true;
			this.lbchild.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.lbchild.Location = new System.Drawing.Point(54, 268);
			this.lbchild.Name = "lbchild";
			this.lbchild.Size = new System.Drawing.Size(47, 20);
			this.lbchild.TabIndex = 5;
			this.lbchild.Text = "Child";
			// 
			// lbexperience
			// 
			this.lbexperience.AutoSize = true;
			this.lbexperience.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.lbexperience.Location = new System.Drawing.Point(54, 317);
			this.lbexperience.Name = "lbexperience";
			this.lbexperience.Size = new System.Drawing.Size(92, 20);
			this.lbexperience.TabIndex = 5;
			this.lbexperience.Text = "Experience";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label7.Location = new System.Drawing.Point(84, 33);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(575, 20);
			this.label7.TabIndex = 6;
			this.label7.Text = "Hint : if you want to change a percentage, Please, click it from combo boxes";
			// 
			// txtbxBaseSalary
			// 
			this.txtbxBaseSalary.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.txtbxBaseSalary.Location = new System.Drawing.Point(174, 374);
			this.txtbxBaseSalary.Name = "txtbxBaseSalary";
			this.txtbxBaseSalary.Size = new System.Drawing.Size(100, 26);
			this.txtbxBaseSalary.TabIndex = 7;
			this.txtbxBaseSalary.TextChanged += new System.EventHandler(this.txtbxBaseSalary_TextChanged);
			this.txtbxBaseSalary.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtboxes_KeyPress);
			// 
			// lbBaseSalary
			// 
			this.lbBaseSalary.AutoSize = true;
			this.lbBaseSalary.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.lbBaseSalary.Location = new System.Drawing.Point(55, 377);
			this.lbBaseSalary.Name = "lbBaseSalary";
			this.lbBaseSalary.Size = new System.Drawing.Size(100, 20);
			this.lbBaseSalary.TabIndex = 8;
			this.lbBaseSalary.Text = "Base Salary";
			// 
			// txtbxShowper
			// 
			this.txtbxShowper.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.txtbxShowper.Location = new System.Drawing.Point(824, 72);
			this.txtbxShowper.Name = "txtbxShowper";
			this.txtbxShowper.Size = new System.Drawing.Size(144, 26);
			this.txtbxShowper.TabIndex = 9;
			this.txtbxShowper.TextChanged += new System.EventHandler(this.txtbxShowper_TextChanged);
			this.txtbxShowper.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtboxes_KeyPress);
			// 
			// lbPercentage
			// 
			this.lbPercentage.AutoSize = true;
			this.lbPercentage.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.lbPercentage.Location = new System.Drawing.Point(705, 72);
			this.lbPercentage.Name = "lbPercentage";
			this.lbPercentage.Size = new System.Drawing.Size(94, 20);
			this.lbPercentage.TabIndex = 10;
			this.lbPercentage.Text = "Percentage";
			// 
			// bttnSave
			// 
			this.bttnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
			this.bttnSave.Location = new System.Drawing.Point(1000, 70);
			this.bttnSave.Name = "bttnSave";
			this.bttnSave.Size = new System.Drawing.Size(90, 28);
			this.bttnSave.TabIndex = 11;
			this.bttnSave.Text = "Save";
			this.bttnSave.UseVisualStyleBackColor = true;
			this.bttnSave.Click += new System.EventHandler(this.bttnSave_Click);
			// 
			// ChangePercentageOfBMO
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1141, 522);
			this.Controls.Add(this.bttnSave);
			this.Controls.Add(this.lbPercentage);
			this.Controls.Add(this.txtbxShowper);
			this.Controls.Add(this.lbBaseSalary);
			this.Controls.Add(this.txtbxBaseSalary);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.lbexperience);
			this.Controls.Add(this.lbchild);
			this.Controls.Add(this.lbposition);
			this.Controls.Add(this.lblanguage);
			this.Controls.Add(this.lbdegree);
			this.Controls.Add(this.lbcity);
			this.Controls.Add(this.cmbxexperience);
			this.Controls.Add(this.cmbxchild);
			this.Controls.Add(this.cmbxposition);
			this.Controls.Add(this.cmbxlanguage);
			this.Controls.Add(this.cmbxdegree);
			this.Controls.Add(this.cmbxcity);
			this.Name = "ChangePercentageOfBMO";
			this.Text = "Change Percentage Of BMO";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ChangePercentageOfBMO_FormClosing);
			this.Load += new System.EventHandler(this.ChangePercentageOfBMO_Load);
			this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtboxes_KeyPress);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ComboBox cmbxcity;
		private System.Windows.Forms.ComboBox cmbxdegree;
		private System.Windows.Forms.ComboBox cmbxlanguage;
		private System.Windows.Forms.ComboBox cmbxposition;
		private System.Windows.Forms.ComboBox cmbxchild;
		private System.Windows.Forms.ComboBox cmbxexperience;
		private System.Windows.Forms.Label lbcity;
		private System.Windows.Forms.Label lbdegree;
		private System.Windows.Forms.Label lblanguage;
		private System.Windows.Forms.Label lbposition;
		private System.Windows.Forms.Label lbchild;
		private System.Windows.Forms.Label lbexperience;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox txtbxBaseSalary;
		private System.Windows.Forms.Label lbBaseSalary;
		private System.Windows.Forms.TextBox txtbxShowper;
		private System.Windows.Forms.Label lbPercentage;
		private System.Windows.Forms.Button bttnSave;
	}
}