﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prelab
{
	class BMO
	{
		public BMO(int marriage, int statusPartner, int noofchildren, int Nooflanguage, int[] Degreeindexs, string city, int ExperienceIndex,int[]foreginindex,int[]children,int positionindex)
		{
			this.marriage = marriage;
			this.statusofpartner = statusPartner;
			this.nofLang = Nooflanguage;
			this.degreeindex = Degreeindexs;
			this.City = city;
			this.experienceindex = ExperienceIndex;
			this.forglangindexes = foreginindex;
			this.children = children;
			this.positionindex = positionindex;
		}


		private int marriage = 0;//if married 1
		private int statusofpartner;
		private int nofchildren = 0;
		private int nofLang = 0;
		private int[] forglangindexes;
		private int[] degreeindex;
		private string city;
		private int experienceindex;
		private int positionindex = -1;
		private int [] children;

		public int[] Children
		{
			get { return children; }
			set { children = value; }
		}

		public int[] Forglangindexes
		{
			get { return forglangindexes; }
			set { forglangindexes = value; }
		}

		public int Positionindex
		{
			get { return positionindex; }
			set { positionindex = value; }
		}

		public int Marriage
		{
			get { return marriage; }
			set { marriage = value; }
		}
		public int StatusofPartner
		{
			get { return statusofpartner; }
			set { statusofpartner = value; }
		}
		public int Nofchildren
		{
			get { return nofchildren; }
			set { nofchildren = value; }
		}
		public int NofLang
		{
			get { return nofLang; }
			set { nofLang = value; }
		}
		public int[] DegreeIndex
		{
			get { return degreeindex; }
			set { degreeindex = value; }
		}
		public string City
		{
			get { return city; }
			set { city = value; }
		}
		public int ExperienceIndex
		{
			get { return experienceindex; }
			set { experienceindex = value; }
		}
	}
}
