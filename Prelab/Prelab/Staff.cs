﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;
using Newtonsoft.Json;
namespace Prelab
{
	class Staff 
	{
		public Staff(string Name, string Surname, int Salary, BMO BMO,int Id)
		{
			this.name = Name;
			this.surname = Surname;
			this.salary = Salary;
			this.bmo = BMO;
			this.id = Id;			
		}
																							
		~Staff()
		{
		}

		public string ToString( char seperated)
		{
			string degreeindex = "";
			if (Bmo.DegreeIndex != null)
			{
				for (int i = 0; i < Bmo.DegreeIndex.Length; i++)
				{
					degreeindex += Bmo.DegreeIndex[i].ToString();
					degreeindex += seperated;
				}
			}
			if (degreeindex == "")
			{
				degreeindex += seperated;
			}
			string foregninx = "";
			if (Bmo.Forglangindexes != null)
			{
				for (int i = 0; i < Bmo.Forglangindexes.Length; i++)
				{
					foregninx += Bmo.Forglangindexes[i];
					foregninx += seperated;
				}
			}
			if (foregninx == "")
			{
				foregninx += seperated;
			}

			string children = "";
			if (Bmo.Children != null)
			{
				for (int i = 0; i < Bmo.Children.Length; i++)
				{
					children += Bmo.Children[i];
					children += seperated;
				}

			}
			if (children == "")
			{
				children += seperated;
			}
			int x = Bmo.NofLang == null ? 0 : bmo.NofLang;
			int y = Bmo.DegreeIndex == null ? 0 : bmo.DegreeIndex.Length;
			int z = Bmo.Forglangindexes == null ? 0 : bmo.Forglangindexes.Length;
			int t = Bmo.Children == null ? 0 : Bmo.Children.Length;
			int r = Bmo.Positionindex == null ? -1 : Bmo.Positionindex;
			return (Name + seperated + surname + seperated + Salary + seperated + Bmo.Marriage + seperated + Bmo.StatusofPartner + seperated + x + seperated + y + seperated + degreeindex + Bmo.City + seperated + Bmo.ExperienceIndex + seperated + z + seperated + foregninx + t + seperated + children + r + seperated + ID);

		}

		protected int id;
		public int ID//Id of staf
		{
			get { return id; }
			set { id = value; }
		}

		protected string name;
		public string Name//name of the staff
		{
			get { return name; }
			set { name = value; }
		}

		protected string surname;
		public string Surname//Surname of the staff
		{
			get { return surname; }
			set { surname = value; }
		}

		protected int salary;
		public int Salary//Salary of the staff
		{
			get { return salary; }
			set { salary = value; }
		}

		protected BMO bmo;
		public BMO Bmo//BMO salary information of the staff
		{
			get { return bmo; }
			set { bmo = value; }
		}
		public virtual double calculatebmo()
		{
			double bmo = 0;
			if (Bmo.StatusofPartner != -1 && Bmo.StatusofPartner == 0)
			{
				bmo += 0.2;
			}
			if (Bmo.Children != null)//children classıı
			{
				for (int i = 0; i < Bmo.Children.Length; i++)
				{
					bmo += Program.chidren.Values.ElementAt(Bmo.Children[i]);
				}

			}
			if (Bmo.Positionindex != -1)
			{
				bmo += Program.position.Values.ElementAt(Bmo.Positionindex);
			}
			if (Bmo.Forglangindexes != null)
			{
				for (int i = 0; i < Bmo.Forglangindexes.Length; i++)
				{
					if (Bmo.Forglangindexes[i] == 2)
					{
						bmo += Program.language.Values.ElementAt(Bmo.Forglangindexes[i]) * Bmo.NofLang;
					}
					else
						bmo += Program.language.Values.ElementAt(Bmo.Forglangindexes[i]);
				}
			}
			bmo += Program.city[Bmo.City];
			bmo += Program.experience.Values.ElementAt(Bmo.ExperienceIndex);
			double Maxx = 0.0;
			if (Bmo.DegreeIndex != null)
			{
				for (int i = 0; i < Bmo.DegreeIndex.Length; i++)
				{
					if (Program.degree.Values.ElementAt(Bmo.DegreeIndex[i]) > Maxx)
					{
						Maxx = Program.degree.Values.ElementAt(Bmo.DegreeIndex[i]);
					}
				}
			}
			bmo += Maxx;
			return Program.baseSalary * (bmo + 1);
		}
	}
}
