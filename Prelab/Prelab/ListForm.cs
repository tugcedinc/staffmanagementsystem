﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace Prelab
{
	public partial class ListForm : Form
	{
		string path = "";

		int index;

		RadioButton bttnevlilik;

		RadioButton es;

		int bttn;

		public ListForm(int index, int bttn)
		{
			InitializeComponent();
			//string citylist = "Adana Adıyaman Afyon Ağrı Amasya Ankara Antalya Artvin Aydın Balıkesir Bilecik Bingöl Bitlis Bolu Burdur Bursa Çanakkale Çankırı Çorum Denizli Diyarbakır Edirne Elazığ Erzincan Erzurum Eskişehir Gaziantep Giresun Gümüşhane Hakkari Hatay Isparta İçel(Mersin) İstanbul İzmir Kars Kastamonu Kayseri Kırklareli Kırşehir Kocaeli Konya Kütahya Malatya Manisa K.maraş Mardin Muğla Muş Nevşehir Niğde Ordu Rize Sakarya Samsun Siirt Sinop Sivas Tekirdağ Tokat Trabzon Tunceli Şanlıurfa Uşak Van Yozgat Zonguldak Aksaray Bayburt Karaman Kırıkkale Batman Şırnak Bartın Ardahan Iğdır Yalova Karabük Kilis Osmaniye Düzce";
			//string[] ctylisst = citylist.Split(' ');
			//for (int i = 0; i < 81; i++)
			//{
			//	cbxcities.Items.Add(ctylisst[i]);
			//}
			chclstbxdegree.DataSource = new BindingSource(Program.degree, null);
			chclstbxdegree.DisplayMember = "Key";
			chclstbxdegree.ValueMember = "Value";
			/************************************************************/
			chclstbxdlanguage.DataSource = new BindingSource(Program.language, null);
			chclstbxdlanguage.DisplayMember = "Key";
			chclstbxdlanguage.ValueMember = "Value";
			/************************************************************/

			/************************************************************/
			cbxposition.DataSource = new BindingSource(Program.position, null);
			cbxposition.DisplayMember = "Key";
			cbxposition.ValueMember = "Value";
			cbxposition.SelectedIndex = -1;
			/************************************************************/

			/************************************************************/
			cbx1stchild.DataSource = new BindingSource(Program.chidren, null);
			cbx1stchild.DisplayMember = "Key";
			cbx1stchild.ValueMember = "Value";
			cbx2ndchild.DataSource = new BindingSource(Program.chidren, null);
			cbx2ndchild.DisplayMember = "Key";
			cbx2ndchild.ValueMember = "Value";
			/************************************************************/

			/************************************************************/
			cbxcities.DataSource = new BindingSource(Program.city, null);
			cbxcities.DisplayMember = "Key";
			cbxcities.ValueMember = "Value";
			/************************************************************/

			/************************************************************/
			cbxexperience.DataSource = new BindingSource(Program.experience, null);
			cbxexperience.DisplayMember = "Key";
			cbxexperience.ValueMember = "Value";
			/************************************************************/
			this.bttn = bttn;
			this.index = index;
			switch (bttn)
			{
				case 1:
					break;
				case 2:
					dtgrdvstafflist_CellContentClick(index);
					break;
				case 3:
					dtgrdvstafflist_CellContentClick(index);
					bttnsave.Text = "Exit";
					break;
				default:
					break;
			}
		}

		/// <summary>
		/// adds the information to list
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private bool bttnAdd_Click()
		{
			try
			{
				int[] degree = null;
				if (chclstbxdegree.CheckedItems.Count != 0)
				{
					degree = new int[chclstbxdegree.CheckedItems.Count];
					for (int i = 0; i < chclstbxdegree.CheckedItems.Count; i++)
					{
						degree[i] = chclstbxdegree.Items.IndexOf(chclstbxdegree.CheckedItems[i]);
					}
				}

				int[] language = null;
				if (chclstbxdlanguage.CheckedItems.Count != 0)
				{
					language = new int[chclstbxdlanguage.CheckedItems.Count];
					for (int i = 0; i < chclstbxdlanguage.CheckedItems.Count; i++)
					{
						language[i] = chclstbxdlanguage.Items.IndexOf(chclstbxdlanguage.CheckedItems[i]);
					}
				}

				int[] children = null;

				if (cbxNoofchildren.SelectedIndex == 1)
				{
					children = new int[1];
					children[0] = cbx1stchild.SelectedIndex;
				}
				else if (cbxNoofchildren.SelectedIndex == 2)
				{
					children = new int[2];
					children[0] = cbx1stchild.SelectedIndex;
					children[1] = cbx2ndchild.SelectedIndex;
				}

				if (rdbFullTimeStaff.Checked == true)
				{
					Program.staff.Add(new Staff(txtbxName.Text, txtbxSurname.Text, Convert.ToInt32(txtbxSalary.Text == "" ? "0" : txtbxSalary.Text), new BMO((bttnevlilik.Text == "Evet" ? 1 : 0), (bttnevlilik.Text == "Evet" ? (es.Text == "Evet" ? 1 : 0) : -1), cbxNoofchildren.SelectedIndex, Convert.ToInt32(txtbxnoofforlang.Text == "" ? "0" : txtbxnoofforlang.Text), degree, Program.city.ElementAt(cbxcities.SelectedIndex).Key , cbxexperience.SelectedIndex, language, children, cbxposition.SelectedIndex), Program.FullTimeStaffIndex++));
				}
				if (rdbParttimeStaff.Checked == true)
				{
					Program.staff.Add(new PartTimeStaff(txtbxName.Text, txtbxSurname.Text, Convert.ToInt32(txtbxSalary.Text == "" ? "0" : txtbxSalary.Text), new BMO((bttnevlilik.Text == "Evet" ? 1 : 0), (bttnevlilik.Text == "Evet" ? (es.Text == "Evet" ? 1 : 0) : -1), cbxNoofchildren.SelectedIndex, Convert.ToInt32(txtbxnoofforlang.Text == "" ? "0" : txtbxnoofforlang.Text), degree, Program.city.ElementAt(cbxcities.SelectedIndex).Key , cbxexperience.SelectedIndex , language, children, cbxposition.SelectedIndex), Program.PartTimeStaffIndex++));
				}
				Console.WriteLine(Program.staff[0].calculatebmo());
				if(path=="")
				{
					path = "pp.jpg";
				}

				int inx = Program.staff.Count;
				inx--;
				string ppp = "D:\\StaffManagementSystem\\" + Program.staff[inx].ID.ToString() + ".jpg";

				Console.WriteLine(Program.staff[inx].calculatebmo());
				File.Copy(path, ppp);
				path = "";
				MessageBox.Show("Staff saved succesfully", " Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return true;
			}
			catch 
			{
				MessageBox.Show("Same areas were empty", "Error",MessageBoxButtons.OK, MessageBoxIcon.Error);
				return false;
			}
		}

		/// <summary>
		/// initiallize the components' values
		/// </summary>
		private void initializecomponents()
		{
			txtbxID.Text = "";
			txtbxName.Text = "";
			txtbxSurname.Text = "";
			txtbxSalary.Text = "";
			txtbxBMO.Text = "";
			rbesevet.Checked = false;
			rbeshayir.Checked = false;
			rbevlilikevet.Checked = false;
			rbevlilikhayir.Checked = false;
			gbes.Visible = false;
			lbuyarı.Visible = false;
			lbchildrens1.Visible = false;
			lbchildrens2.Visible = false;
			cbx1stchild.SelectedIndex = -1;
			cbx2ndchild.SelectedIndex = -1;
			cbxcities.SelectedIndex = -1;
			cbxexperience.SelectedIndex = -1;
			for (int i = 0; i < chclstbxdegree.Items.Count; i++)
			{
				chclstbxdegree.SetItemChecked(i, false);
			}
			for (int i = 0; i < chclstbxdlanguage.Items.Count; i++)
			{
				chclstbxdlanguage.SetItemChecked(i, false);
			}
			lbForeginl.Visible = false;
			txtbxnoofforlang.Text = "";
			txtbxnoofforlang.Visible = false;
			cbxposition.SelectedIndex = -1;
			if (pctrebxfoto.Image != null)
			{
				pctrebxfoto.Image.Dispose();
				pctrebxfoto.Image = null;
			}
		}
				
		private void bttnUpdate_Click(int updateindex)
		{
			try
			{
				if (updateindex != -1)
				{
					int[] degree = null;
					if (chclstbxdegree.CheckedItems.Count != 0)
					{
						degree = new int[chclstbxdegree.CheckedItems.Count];
						for (int i = 0; i < chclstbxdegree.CheckedItems.Count; i++)
						{
							degree[i] = chclstbxdegree.Items.IndexOf(chclstbxdegree.CheckedItems[i]);
						}
					}

					int[] language = null;
					if (chclstbxdlanguage.CheckedItems.Count != 0)
					{
						language = new int[chclstbxdlanguage.CheckedItems.Count];
						for (int i = 0; i < chclstbxdlanguage.CheckedItems.Count; i++)
						{
							language[i] = chclstbxdlanguage.Items.IndexOf(chclstbxdlanguage.CheckedItems[i]);
						}
					}
					int[] children = null;
					if (cbxNoofchildren.SelectedIndex == 1)
					{
						children = new int[1];
						children[0] = cbx1stchild.SelectedIndex;
					}
					else if (cbxNoofchildren.SelectedIndex == 2)
					{
						children = new int[2];
						children[0] = cbx1stchild.SelectedIndex;
						children[1] = cbx2ndchild.SelectedIndex;
					}
				//	Program.staff[updateindex] = new Staff(txtbxName.Text, txtbxSurname.Text, Convert.ToInt32(txtbxSalary.Text == "" ? "0" : txtbxSalary.Text), new BMO((bttnevlilik.Text == "Evet" ? 1 : 0), (bttnevlilik.Text == "Evet" ? (es.Text == "Evet" ? 1 : 0) : -1), cbxNoofchildren.SelectedIndex, Convert.ToInt32(txtbxnoofforlang.Text == "" ? "0" : txtbxnoofforlang.Text), degree, cbxcities.SelectedItem.ToString(), cbxexperience.SelectedIndex, language, children, cbxposition.SelectedIndex), Program.staff[updateindex].ID);
					if (rdbFullTimeStaff.Checked == true)
					{
						Program.staff[updateindex]=(new Staff(txtbxName.Text, txtbxSurname.Text, Convert.ToInt32(txtbxSalary.Text == "" ? "0" : txtbxSalary.Text), new BMO((bttnevlilik.Text == "Evet" ? 1 : 0), (bttnevlilik.Text == "Evet" ? (es.Text == "Evet" ? 1 : 0) : -1), cbxNoofchildren.SelectedIndex, Convert.ToInt32(txtbxnoofforlang.Text == "" ? "0" : txtbxnoofforlang.Text), degree, Program.city.ElementAt(cbxcities.SelectedIndex).Key, cbxexperience.SelectedIndex, language, children, cbxposition.SelectedIndex), Program.FullTimeStaffIndex++));
					}
					if (rdbParttimeStaff.Checked == true)
					{
						Program.staff[updateindex] = (new PartTimeStaff(txtbxName.Text, txtbxSurname.Text, Convert.ToInt32(txtbxSalary.Text == "" ? "0" : txtbxSalary.Text), new BMO((bttnevlilik.Text == "Evet" ? 1 : 0), (bttnevlilik.Text == "Evet" ? (es.Text == "Evet" ? 1 : 0) : -1), cbxNoofchildren.SelectedIndex, Convert.ToInt32(txtbxnoofforlang.Text == "" ? "0" : txtbxnoofforlang.Text), degree, Program.city.ElementAt(cbxcities.SelectedIndex).Key, cbxexperience.SelectedIndex, language, children, cbxposition.SelectedIndex), Program.PartTimeStaffIndex++));
					}

					string ppp = "D:\\StaffManagementSystem\\" + Program.staff[updateindex].ID.ToString() + ".jpg";
					if (path != "")
					{

						FileInfo fi = new FileInfo(ppp);
						fi.Delete();
						fi.Refresh();
						fi = null;
						pctrebxfoto.Image.Dispose();


						File.Delete(ppp);
						Console.WriteLine(Program.staff[updateindex].calculatebmo());
						File.Copy(path, ppp);
					}
				}
			}
			catch 
			{
				MessageBox.Show("Same areas were empty", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

			}
		}
				
		private void bttnDelete_Click(int deleteindex)
		{
			try
			{
				string ppp = "D:\\StaffManagementSystem\\" + Program.staff[deleteindex].ID.ToString() + ".jpg";
				Program.staff.RemoveAt(deleteindex);
				pctrebxfoto.Image.Dispose();
				File.Delete(ppp);
				Console.WriteLine(Program.staff[deleteindex].calculatebmo());
				initializecomponents();
			}
			catch (Exception exp)
			{
				MessageBox.Show(exp.Message);
			}
		}

		/// <summary>
		/// it shows the information of the selected staff to the components.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cbxNoofchildren_SelectedIndexChanged(object sender, EventArgs e)
		{
			if(cbxNoofchildren.SelectedIndex==1)
			{
				lbchildrens1.Visible = true;
				cbx1stchild.Visible = true;
				lbchildrens2.Visible = false;
				cbx2ndchild.Visible = false;
			}
			else if(cbxNoofchildren.SelectedIndex==2)
			{
				lbuyarı.Visible = true;
				lbchildrens1.Visible = true;
				cbx1stchild.Visible = true;
				lbchildrens2.Visible = true;
				cbx2ndchild.Visible = true;
			}
			else
			{
				lbchildrens1.Visible = false;
				cbx1stchild.Visible = false;
				lbchildrens2.Visible = false;
				cbx2ndchild.Visible = false;
			}
		}
		
		/// <summary>
		/// it checks the staff whether maried or not.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void evlilgrupbx(object sender, EventArgs e)
		{
			RadioButton radioButton = (RadioButton)sender;
			bttnevlilik = radioButton;
			if (radioButton.Text == "Evet")
			{
				gbes.Visible = true;
			}
			else
				gbes.Visible = false;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void chclstbxdlanguage_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				if (chclstbxdlanguage.CheckedItems.Contains(chclstbxdlanguage.Items[2]))
				{
					lbForeginl.Visible = true;
					txtbxnoofforlang.Visible = true;
				}
				if (!chclstbxdlanguage.CheckedItems.Contains(chclstbxdlanguage.Items[2]))
				{
					lbForeginl.Visible = false;
					txtbxnoofforlang.Visible = false;
				}
			}
			catch
			{

			}
		}

		/// <summary>
		/// it checks the staff's partner whether working or not.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void esgrupbx(object sender, EventArgs e)
		{
			es = (RadioButton)sender;
		}
		
		/// <summary>
		/// it selects the picture of staff
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void button1_Click(object sender, EventArgs e)
		{
			OpenFileDialog file = new OpenFileDialog();
			file.Title = "Dosyayı Seçin";
			file.Filter = "Image Files(*.jpg)|*.jpg";
			
			DialogResult result = file.ShowDialog();
			if (result == DialogResult.OK)
			{
				path = file.FileName;
				if (pctrebxfoto.Image != null)
				{
					pctrebxfoto.Image.Dispose();
				}
				pctrebxfoto.Image = new Bitmap(path);
				pctrebxfoto.SizeMode = PictureBoxSizeMode.Zoom;
			}
			file.Dispose();
		}

		/// <summary>
		/// it works when the datagrid row selected.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void dtgrdvstafflist_CellContentClick(int updateindex)
		{
			//initializecomponents();
			if (updateindex >= 0)
			{
				//t.Equals(typeof(long))
				if (Program.staff[updateindex].GetType()== typeof(PartTimeStaff))
				{
					rdbParttimeStaff.Checked = true;
				}
				else
				{
					rdbFullTimeStaff.Checked = true;
				}
				txtbxID.Text = Program.staff[updateindex].ID.ToString();
				txtbxName.Text = Program.staff[updateindex].Name;
				txtbxSurname.Text = Program.staff[updateindex].Surname;
				txtbxSalary.Text = Program.staff[updateindex].Salary.ToString();
				txtbxBMO.Text = Program.staff[updateindex].calculatebmo().ToString();
				cbxcities.SelectedItem = Program.staff[updateindex].Bmo.City;
				cbxexperience.SelectedIndex = Program.staff[updateindex].Bmo.ExperienceIndex;
				if (Program.staff[updateindex].Bmo.Marriage == 1)
				{
					rbevlilikevet.Select();
					gbes.Visible = true;
					if (Program.staff[updateindex].Bmo.StatusofPartner == 1)
					{
						rbesevet.Select();
					}
					else
					{
						rbeshayir.Select();
					}
				}
				else
				{
					rbevlilikhayir.Select();
					gbes.Visible = false;

				}
				if (Program.staff[updateindex].Bmo.Children != null)
				{
					cbxNoofchildren.SelectedIndex = Program.staff[updateindex].Bmo.Children.Length;
					if (Program.staff[updateindex].Bmo.Children.Length == 1)
					{
						cbx1stchild.Visible = true;
						cbx2ndchild.Visible = false;
						lbchildrens1.Visible = true;
						lbchildrens2.Visible = false;
						cbx1stchild.SelectedIndex = Program.staff[updateindex].Bmo.Children[0];

					}
					else if (Program.staff[updateindex].Bmo.Children.Length == 2)
					{
						cbx1stchild.Visible = true;
						cbx2ndchild.Visible = true;
						lbchildrens1.Visible = true;
						lbchildrens2.Visible = true;
						cbx1stchild.SelectedIndex = Program.staff[updateindex].Bmo.Children[0];
						cbx2ndchild.SelectedIndex = Program.staff[updateindex].Bmo.Children[1];

					}
					else
					{
						cbx1stchild.Visible = false;
						cbx2ndchild.Visible = false;
						lbchildrens1.Visible = false;
						lbchildrens2.Visible = false;
					}
				}
				else
					cbxNoofchildren.SelectedIndex = 0;
				cbxNoofchildren.Visible = true;
				if (Program.staff[updateindex].Bmo.DegreeIndex != null)
				{
					for (int i = 0; i < Program.staff[updateindex].Bmo.DegreeIndex.Length; i++)
					{
						chclstbxdegree.SetItemChecked(Program.staff[updateindex].Bmo.DegreeIndex[i], true);
						//chclstbxdegree.SetSelected//[Program.staff[updateindex].Bmo.DegreeIndex[i]]
					}
				}

				if (Program.staff[updateindex].Bmo.Forglangindexes != null)
				{
					for (int i = 0; i < Program.staff[updateindex].Bmo.Forglangindexes.Length; i++)
					{
						chclstbxdlanguage.SetItemChecked(Program.staff[updateindex].Bmo.Forglangindexes[i], true);
						if (Program.staff[updateindex].Bmo.Forglangindexes[i] == 2)
						{
							lbForeginl.Visible = true;
							txtbxnoofforlang.Visible = true;
							txtbxnoofforlang.Text = Program.staff[updateindex].Bmo.NofLang.ToString();

						}
					}
				}

				cbxposition.SelectedIndex = Program.staff[updateindex].Bmo.Positionindex;

				try
				{

					string ppp = "D:\\StaffManagementSystem\\" + Program.staff[updateindex].ID.ToString() + ".jpg";
					pctrebxfoto.Image = new Bitmap(ppp);
					pctrebxfoto.SizeMode = PictureBoxSizeMode.Zoom;
				}
				catch
				{
					string pyy = "pp.jpg";
					string ppp = "D:\\StaffManagementSystem\\" + Program.staff[updateindex].ID.ToString() + ".jpg";
					File.Copy(pyy, ppp);
					pctrebxfoto.Image = new Bitmap(ppp);
					pctrebxfoto.SizeMode = PictureBoxSizeMode.Zoom;
				}
			}
		}

		private void bttnClear_Click(object sender, EventArgs e)
		{
			initializecomponents();
		}

		private void txtbxSalary_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (!(char.IsDigit(e.KeyChar) || char.IsControl(e.KeyChar)))
			{
				e.Handled = true;
			}
		}

		private void bttnsave_Click(object sender, EventArgs e)
		{
			
			switch (bttn)
			{
				case 1:
					if(bttnAdd_Click()==false)
					{
						return;
					}
					break;
				case 2:
					bttnUpdate_Click(index);
					break;
				case 3:
					break;
				default:
					break;
			}
			if (pctrebxfoto.Image != null)
			{
				pctrebxfoto.Image.Dispose();
			}
			this.Close();

			this.Dispose();
		}

		private void ListForm_Load(object sender, EventArgs e)
		{
			
		}
	}
}
