﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prelab
{
	class PartTimeStaff : Staff
	{
		public PartTimeStaff(string Name, string Surname, int Salary, BMO BMO, int Id) : base(Name, Surname, Salary, BMO, Id)
		{

		}
		public override double calculatebmo()
		{
			double bmo = 0;
			if (Bmo.StatusofPartner != -1 && Bmo.StatusofPartner == 0)
			{
				bmo += 0.2;
			}
			if (Bmo.Children != null)//children classıı
			{
				for (int i = 0; i < Bmo.Children.Length; i++)
				{
					bmo += Program.chidren.Values.ElementAt(Bmo.Children[i]);
				}

			}
			if (Bmo.Positionindex != -1)
			{
				bmo += Program.position.Values.ElementAt(Bmo.Positionindex);
			}
			if (Bmo.Forglangindexes != null)
			{
				for (int i = 0; i < Bmo.Forglangindexes.Length; i++)
				{
					if (Bmo.Forglangindexes[i] == 2)
					{
						bmo += Program.language.Values.ElementAt(Bmo.Forglangindexes[i]) * Bmo.NofLang;
					}
					else
						bmo += Program.language.Values.ElementAt(Bmo.Forglangindexes[i]);
				}
			}
			bmo += Program.city[Bmo.City];
			bmo += Program.experience.Values.ElementAt(Bmo.ExperienceIndex);
			double Maxx = 0.0;
			if (Bmo.DegreeIndex != null)
			{
				for (int i = 0; i < Bmo.DegreeIndex.Length; i++)
				{
					if (Program.degree.Values.ElementAt(Bmo.DegreeIndex[i]) > Maxx)
					{
						Maxx = Program.degree.Values.ElementAt(Bmo.DegreeIndex[i]);
					}
				}
			}
			bmo += Maxx;
			return Program.baseSalary * (bmo + 1) / 2;
		}
	}
}
