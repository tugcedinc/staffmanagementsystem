﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prelab
{
	using Newtonsoft.Json;
	using System.IO;

	public partial class ChangePercentageOfBMO : Form
	{
		public ChangePercentageOfBMO()
		{
			InitializeComponent();
		}

		private void ChangePercentageOfBMO_Load(object sender, EventArgs e)
		{
			/************************************************************/
			cmbxdegree.DataSource = new BindingSource(Program.degree, null);
			cmbxdegree.DisplayMember = "Key";
			cmbxdegree.ValueMember = "Value";
			cmbxdegree.SelectedIndex = -1;
			/************************************************************/

			/************************************************************/
			cmbxlanguage.DataSource = new BindingSource(Program.language, null);
			cmbxlanguage.DisplayMember = "Key";
			cmbxlanguage.ValueMember = "Value";
			cmbxlanguage.SelectedIndex = -1;

			/************************************************************/

			/************************************************************/
			cmbxposition.DataSource = new BindingSource(Program.position, null);
			cmbxposition.DisplayMember = "Key";
			cmbxposition.ValueMember = "Value";
			cmbxposition.SelectedIndex = -1;
			/************************************************************/

			/************************************************************/
			cmbxchild.DataSource = new BindingSource(Program.chidren, null);
			cmbxchild.DisplayMember = "Key";
			cmbxchild.ValueMember = "Value";
			cmbxchild.SelectedIndex = -1;
			/************************************************************/

			/************************************************************/
			cmbxcity.DataSource = new BindingSource(Program.city, null);
			cmbxcity.DisplayMember = "Key";
			cmbxcity.ValueMember = "Value";
			cmbxcity.SelectedIndex = -1;
			/************************************************************/

			/************************************************************/
			cmbxexperience.DataSource = new BindingSource(Program.experience, null);
			cmbxexperience.DisplayMember = "Key";
			cmbxexperience.ValueMember = "Value";
			cmbxexperience.SelectedIndex = -1;
			/************************************************************/
			txtbxBaseSalary.Text = Program.baseSalary.ToString();
		}

		int selectedindex = -1;
		ComboBox comboBox;
		private void AllComboBoxes_SelectedIndexChanged(object sender, EventArgs e)
		{
			comboBox = (ComboBox)sender;
			int index = selectedindex = comboBox.SelectedIndex;
			if (index != -1)
			{
				if (comboBox.Name == "cmbxcity")
				{
					txtbxShowper.Text = Program.city.ElementAt(index).Value.ToString();
				}
				else if (comboBox.Name == "cmbxdegree")
				{
					txtbxShowper.Text = Program.degree.ElementAt(index).Value.ToString();
				}
				else if (comboBox.Name == "cmbxlanguage")
				{
					txtbxShowper.Text = Program.language.ElementAt(index).Value.ToString();
				}
				else if (comboBox.Name == "cmbxposition")
				{
					txtbxShowper.Text = Program.position.ElementAt(index).Value.ToString();
				}
				else if (comboBox.Name == "cmbxchild")
				{
					txtbxShowper.Text = Program.chidren.ElementAt(index).Value.ToString();
				}
				else if (comboBox.Name == "cmbxexperience")
				{
					txtbxShowper.Text = Program.experience.ElementAt(index).Value.ToString();
				}
				else
				{
					txtbxShowper.Text = "";
				}
			}
		}

		private void txtbxShowper_TextChanged(object sender, EventArgs e)
		{

		}

		private void bttnSave_Click(object sender, EventArgs e)
		{
			if (comboBox != null)
			{
				try
				{
					if (comboBox.Name == "cmbxcity")
					{
						Program.city[Program.city.ElementAt(selectedindex).Key] = Convert.ToDouble(txtbxShowper.Text);
					}
					else if (comboBox.Name == "cmbxdegree")
					{
						Program.degree[Program.degree.ElementAt(selectedindex).Key] = Convert.ToDouble(txtbxShowper.Text);
					}
					else if (comboBox.Name == "cmbxlanguage")
					{
						Program.language[Program.language.ElementAt(selectedindex).Key] = Convert.ToDouble(txtbxShowper.Text);
					}
					else if (comboBox.Name == "cmbxposition")
					{
						Program.position[Program.position.ElementAt(selectedindex).Key] = Convert.ToDouble(txtbxShowper.Text);
					}
					else if (comboBox.Name == "cmbxchild")
					{
						Program.chidren[Program.chidren.ElementAt(selectedindex).Key] = Convert.ToDouble(txtbxShowper.Text);
					}
					else if (comboBox.Name == "cmbxexperience")
					{
						Program.experience[Program.experience.ElementAt(selectedindex).Key] = Convert.ToDouble(txtbxShowper.Text);
					}
					else
					{
						txtbxShowper.Text = "";
					}
				}
				catch
				{ }
			}
		}

		private void txtboxes_KeyPress(object sender, KeyPressEventArgs e)
		{
			TextBox txtbx = (TextBox)sender;
			if (!(char.IsControl(e.KeyChar) || char.IsDigit(e.KeyChar) || e.KeyChar == ','))
			{
				e.Handled = true;
			}
			else
			{
				if (txtbx.Text.Contains(',') && e.KeyChar == ',')
				{
					e.Handled = true;
				}
			}
		}

		private void ChangePercentageOfBMO_FormClosing(object sender, FormClosingEventArgs e)
		{
			DialogResult result = MessageBox.Show("Do you want to save changes?", "Staff Management System", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
			if (result == DialogResult.Yes)
			{
				/************************************************************/
				string stringss = JsonConvert.SerializeObject(Program.degree);
				File.WriteAllText(@"degree.dat", stringss);
				/************************************************************/

				/************************************************************/
				stringss = JsonConvert.SerializeObject(Program.city);
				File.WriteAllText(@"cities.dat", stringss);
				/************************************************************/

				/************************************************************/
				stringss = JsonConvert.SerializeObject(Program.position);
				File.WriteAllText(@"position.dat", stringss);
				/************************************************************/

				/************************************************************/
				stringss = JsonConvert.SerializeObject(Program.chidren);
				File.WriteAllText(@"chidren.dat", stringss);
				/************************************************************/

				/************************************************************/
				stringss = JsonConvert.SerializeObject(Program.language);
				File.WriteAllText(@"language.dat", stringss);
				/************************************************************/

				/************************************************************/
				stringss = JsonConvert.SerializeObject(Program.experience);
				File.WriteAllText(@"experience.dat", stringss);
				/************************************************************/

				/************************************************************/
				Program.baseSalary = Convert.ToInt32(txtbxBaseSalary.Text);
				stringss = JsonConvert.SerializeObject(Program.baseSalary);
				File.WriteAllText(@"basesalary.dat",stringss);
				/************************************************************/
			}
			else
			{
				//this for the BMO calculation for city************************************//
				string citystrings = File.ReadAllText(@"cities.dat");
				Program.city = JsonConvert.DeserializeObject<Dictionary<string, double>>(citystrings);
				//*************************************************************************//


				//this for the BMO calculation for degree**********************************//
				string degreestrings = File.ReadAllText(@"degree.dat");
				Program.degree = JsonConvert.DeserializeObject<Dictionary<string, double>>(degreestrings);
				//*************************************************************************//


				//this for the BMO calculation for experince********************************//
				string experiencestrings = File.ReadAllText(@"experience.dat");
				Program.experience = JsonConvert.DeserializeObject<Dictionary<string, double>>(experiencestrings);
				//*************************************************************************//


				//this for the BMO calculation for language checklistbox*******************//
				string languagestrings = File.ReadAllText(@"language.dat");
				Program.language = JsonConvert.DeserializeObject<Dictionary<string, double>>(languagestrings);
				//*************************************************************************//


				//this for the BMO calculation for position********************************//
				string positionstrings = File.ReadAllText(@"position.dat");
				Program.position = JsonConvert.DeserializeObject<Dictionary<string, double>>(positionstrings);
				//*************************************************************************//


				//this for the BMO calculation for children ages***************************//
				string chidrenstrings = File.ReadAllText(@"chidren.dat");
				Program.chidren = JsonConvert.DeserializeObject<Dictionary<string, double>>(chidrenstrings);
				//*************************************************************************//
				
				string basesalarystring = File.ReadAllText(@"basesalary.dat");
				Program.baseSalary = JsonConvert.DeserializeObject<int>(basesalarystring);
							   				 
			}
		}

		private void txtbxBaseSalary_TextChanged(object sender, EventArgs e)
		{

		}
	}
}
