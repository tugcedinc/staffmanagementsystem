﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prelab
{
	class MergeSort
	{
		public static List<Staff> Mergesort(List<Staff> unsorted)
		{
			if (unsorted.Count <= 1)
				return unsorted;

			List<Staff> left = new List<Staff>();
			List<Staff> right = new List<Staff>();

			int middle = unsorted.Count / 2;
			for (int i = 0; i < middle; i++)  //Dividing the unsorted list
			{
				left.Add(unsorted[i]);
			}
			for (int i = middle; i < unsorted.Count; i++)
			{
				right.Add(unsorted[i]);
			}

			left = Mergesort(left);
			right = Mergesort(right);
			return Merge(left, right);
		}

		public static List<Staff> Merge(List<Staff> left, List<Staff> right)
		{
			List<Staff> result = new List<Staff>();

			while (left.Count > 0 || right.Count > 0)
			{
				if (left.Count > 0 && right.Count > 0)
				{

					if (left.First().Salary <= right.First().Salary)  //Comparing First two elements to see which is smaller
					{
						result.Add(left.First());
						left.Remove(left.First());      //Rest of the list minus the first element
					}
					else
					{
						result.Add(right.First());
						right.Remove(right.First());
					}

				}
				else if (left.Count > 0)
				{
					result.Add(left.First());
					left.Remove(left.First());
				}
				else if (right.Count > 0)
				{
					result.Add(right.First());

					right.Remove(right.First());
				}
			}
			return result;
		}
	}
}
